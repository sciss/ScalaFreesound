/*
 *  Previews.scala
 *  (ScalaFreesound)
 *
 *  Copyright (c) 2010-2024 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.freesound

import de.sciss.serial.{ConstFormat, DataInput, DataOutput}

import java.net.URI

object Previews {
  private final val SER_VER = 0

  implicit object format extends ConstFormat[Previews] {
    def write(v: Previews, out: DataOutput): Unit = {
      out.writeByte(SER_VER)
      out.writeUTF(v.mp3Lo.toString)
      out.writeUTF(v.mp3Hi.toString)
      out.writeUTF(v.oggLo.toString)
      out.writeUTF(v.oggHi.toString)
    }

    def read(in: DataInput): Previews = {
      val v = in.readByte()
      require (v == SER_VER, s"Unknown Previews serialization version $SER_VER}")
      val mp3Lo = new URI(in.readUTF())
      val mp3Hi = new URI(in.readUTF())
      val oggLo = new URI(in.readUTF())
      val oggHi = new URI(in.readUTF())
      Previews(mp3Lo = mp3Lo, mp3Hi = mp3Hi, oggLo = oggLo, oggHi = oggHi)
    }
  }
}
/** Collection of sound preview URIs for mp3 and ogg in two different qualities.
  */
case class Previews(mp3Lo: URI, mp3Hi: URI, oggLo: URI, oggHi: URI) {
  def uri(ogg: Boolean, hq: Boolean): URI =
    if (ogg) {
      if (hq) oggHi else oggLo
    } else {
      if (hq) mp3Hi else mp3Lo
    }
}
