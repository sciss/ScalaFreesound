/*
 *  SoundObj.scala
 *  (ScalaFreesound)
 *
 *  Copyright (c) 2010-2024 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.freesound.lucre

import de.sciss.freesound
import de.sciss.freesound.Sound
import de.sciss.lucre.Event.Targets
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.impl.ExprTypeImpl
import de.sciss.lucre.{Event, Expr, Ident, Txn, Var => LVar}
import de.sciss.serial.ConstFormat

import scala.collection.immutable.{IndexedSeq => Vec}

object SoundObj extends ExprTypeImpl[Sound, SoundObj] {
  import freesound.lucre.{SoundObj => Repr}

  final val typeId = 201

  final val valueName = "Sound"

  override def defaultValue: A = null

  final val valueFormat: ConstFormat[Sound] = Sound.format

  def tryParse(value: Any): Option[Sound] = value match {
    case s: Sound => Some(s)
    case _        => None
  }

  override protected def mkConst[T <: Txn[T]](id: Ident[T], value: A)(implicit tx: T): Const[T] =
    new _Const[T](id, value)

  override protected def mkVar[T <: Txn[T]](targets: Targets[T], vr: LVar[T, E[T]], connect: Boolean)
                                           (implicit tx: T): Var[T] = {
    val res = new _Var[T](targets, vr)
    if (connect) res.connect()
    res
  }

  override protected def mkProgram[T <: Txn[T]](targets: Targets[T], program: LVar[T, Ex[A]],
                                                sources: LVar[T, Vec[Event[T, Any]]],
                                                value: LVar[T, A], connect: Boolean)
                                               (implicit tx: T): Program[T] =
    throw new UnsupportedOperationException

  private[this] final class _Const[T <: Txn[T]](val id: Ident[T], val constValue: A)
    extends ConstImpl[T] with Repr[T]

  private[this] final class _Var[T <: Txn[T]](val targets: Targets[T], val ref: LVar[T, E[T]])
    extends VarImpl[T] with Repr[T]
}
trait SoundObj[T <: Txn[T]] extends Expr[T, Sound]